"""
This module contains the opcodes of the bytecode.
"""

from enum import Enum


class Opcode(Enum):
    """
    This class contains the opcodes of the bytecode.
    """

    ACC_PUSH  = 10 # acc.push(value)                        arg0: value to push
    ACC_POP   = 11 # acc.pop()
    GVAR_DECL = 20 # gvar = acc.pop()                       arg0: global variable name
    GVAR_SET  = 21 # gvar = acc[-1]                         arg0: global variable name
    GVAR_GET  = 22 # acc.push(gvar)                         arg0: global variable name
    LVAR_DECL = 30 # lvar = acc.pop()                       arg0: local variable name
    LVAR_SET  = 31 # lvar = acc[-1]                         arg0: local variable name
    LVAR_GET  = 32 # acc.push(lvar)                         arg0: local variable name
    CALL      = 40 # acc.push(loc); goto addr               arg0: addr, arg1: number of params
    NAT_CALL  = 41 # acc.push(loc); goto name               arg0: name of native function
    RETURN    = 42 # swap_top_two(acc); goto acc.pop()      arg0: number of params
    GOTO      = 43 # goto addr                              arg0: addr
    ELSE_GO   = 44 # if not acc.pop(): goto addr            arg0: addr
    ADD       = 50 # swap_top_two(acc); acc.push(acc.pop() + acc.pop())
    SUB       = 51 # swap_top_two(acc); acc.push(acc.pop() - acc.pop())
    MUL       = 52 # swap_top_two(acc); acc.push(acc.pop() * acc.pop())
    DIV       = 53 # swap_top_two(acc); acc.push(acc.pop() / acc.pop())
    MOD       = 54 # swap_top_two(acc); acc.push(acc.pop() % acc.pop())
    EQ        = 60 # swap_top_two(acc); acc.push(acc.pop() == acc.pop())
    NEQ       = 61 # swap_top_two(acc); acc.push(acc.pop() != acc.pop())
    LT        = 62 # swap_top_two(acc); acc.push(acc.pop() < acc.pop())
    GT        = 63 # swap_top_two(acc); acc.push(acc.pop() > acc.pop())
    LTE       = 64 # swap_top_two(acc); acc.push(acc.pop() <= acc.pop())
    GTE       = 65 # swap_top_two(acc); acc.push(acc.pop() >= acc.pop())
    AND       = 70 # swap_top_two(acc); acc.push(acc.pop() and acc.pop())
    OR        = 71 # swap_top_two(acc); acc.push(acc.pop() or acc.pop())
    NOT       = 72 # acc.push(not acc.pop())
    NEG       = 73 # acc.push(-acc.pop())
    FN_CHECK  = 80 # if acc.pop() != acc.pop(): raise error: nb_params != nb_args
    END       = 99 # end of the program
    INVALID   = 200
    TODO      = 201

    def __str__(self):
        return self.name
