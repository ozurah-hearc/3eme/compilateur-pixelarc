def add_to_class(cls):
    ''' Décorateur permettant d'ajouter la fonction décorée en tant que méthode
    à une classe.
    
    Permet d'implémenter une forme élémentaire de programmation orientée
    aspects en regroupant les méthodes de différentes classes implémentant
    une même fonctionnalité en un seul endroit.
    
    Attention, après utilisation de ce décorateur, la fonction décorée reste dans
    le namespace courant. Si cela dérange, on peut utiliser del pour la détruire.
    Je ne sais pas s'il existe un moyen d'éviter ce phénomène.
    '''
    def decorator(func):
        setattr(cls,func.__name__,func)
        return func
    return decorator


def add_to_dict(dictionary, key):
    ''' Décorateur permettant d'ajouter la fonction décorée en tant que valeur
    à une clé d'un dictionnaire.
    '''
    def decorator(func):
        dictionary[key] = func
        return func
    return decorator
