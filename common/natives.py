"""
Native function definitions for the VM.
"""

import math
from PIL import Image

def fn_abs(vm, x):
    """
    Return the absolute value of a number.
    """

    if x < 0:
        return -x
    return x

def fn_pow(vm, a, b):
    """
    Return a to the power of b.
    """

    return a ** b

def fn_sqrt(vm, x):
    """
    Return the square root of a number.
    """

    return x ** 0.5

def fn_min(vm, a, b):
    """
    Return the minimum of two numbers.
    """

    if a < b:
        return a
    return b

def fn_max(vm, a, b):
    """
    Return the maximum of two numbers.
    """

    if a > b:
        return a
    return b

def fn_sin(vm, x):
    """
    Return the sinus of a number.
    """

    return math.sin(x)

def fn_cos(vm, x):
    """
    Return the cosinus of a number.
    """

    return math.cos(x)

def fn_tan(vm, x):
    """
    Return the tangent of a number.
    """

    return math.tan(x)

def fn_asin(vm, x):
    """
    Return the arc sinus of a number.
    """

    return math.asin(x)

def fn_acos(vm, x):
    """
    Return the arc cosinus of a number.
    """

    return math.acos(x)

def fn_atan(vm, x):
    """
    Return the arc tangent of a number.
    """

    return math.atan(x)

def fn_print(vm, x):
    """
    Print a value.
    """

    print('-->', x)
    return x

def fn_color(vm, color):
    """
    Set the current color.
    """

    vm.set_color(color)
    return color

def fn_line(vm, nb_pixels):
    """
    Draw a line of nb_pixels pixels.
    """

    vm.draw_line(int(nb_pixels))
    return 0.0

def fn_next_x(vm, nb=1):
    """
    Move the cursor forward by nb pixels.
    """

    vm.move_cursor_forward(int(nb))
    return 0.0

def fn_next_y(vm, nb=1):
    """
    Move the cursor downward by nb pixels.
    """
    vm.move_cursor_downward(int(nb))
    return 0.0

def fn_newline(vm, nb=1):
    """
    Move the cursor to the N-th next line.
    """

    vm.move_cursor_newline(int(nb))
    return 0.0

def fn_cursor(vm, x, y):
    """
    Move the cursor to the given coordinates.
    """

    vm.move_cursor_at(int(x), int(y))
    return 0.0
    
def fn_cursor_xy(vm, xy):
    """
    Move the cursor to the given coordinates (same x and y).
    """

    return fn_cursor(vm, xy, xy)
    
def fn_cursor_x(vm, x):
    """
    Move the cursor to the given x coordinate.
    """

    return fn_cursor(vm, x, fn_locator_y(vm))
    
def fn_cursor_y(vm, y):
    """
    Move the cursor to the given y coordinate.
    """

    return fn_cursor(vm, fn_locator_x(vm), y)
    
def fn_locator_x(vm):
    """
    Return the x coordinate of the cursor.
    """

    return vm.get_cursor_x()
    
def fn_locator_y(vm):
    """
    Return the y coordinate of the cursor.
    """

    return vm.get_cursor_y()

def fn_size(vm, width, height):
    """
    Set the size of the canvas.
    """

    vm.set_canvas_size(int(width), int(height))
    return 0.0

def fn_canvas_pixel_size(vm, x_size, y_size):
    """
    Set the size of the canvas in pixels.
    """

    vm.set_canvas_pixel_size(int(x_size), int(y_size))
    return 0.0

def fn_canvas_pixel_size_x(vm, x_size):
    """
    Set the width of the canvas in pixels.
    """

    vm.set_canvas_pixel_size_x(int(x_size))
    return 0.0

def fn_canvas_pixel_size_y(vm, y_size):
    """
    Set the height of the canvas in pixels.
    """

    vm.set_canvas_pixel_size_y(int(y_size))
    return 0.0

def fn_canvas_start(vm, x, y):
    """
    Set the start pixel of the canvas.
    """

    vm.set_canvas_start_pixel(int(x), int(y))
    return 0.0
    
def fn_canvas_start_x(vm, x):
    """
    Set the x-axis start pixel of the canvas.
    """

    vm.set_canvas_start_pixel_x(int(x))
    return 0.0
    
def fn_canvas_start_y(vm, y):
    """
    Set the y-axis start pixel of the canvas.
    """

    vm.set_canvas_start_pixel_y(int(y))
    return 0.0
    
def fn_canvas_end(vm, x, y):
    """
    Set the end pixel of the canvas.
    """

    vm.set_canvas_end_pixel(int(x), int(y))
    return 0.0
    
def fn_canvas_end_x(vm, x):
    """
    Set the x-axis end pixel of the canvas.
    """

    vm.set_canvas_end_pixel_x(int(x))
    return 0.0
    
def fn_canvas_end_y(vm, y):
    """
    Set the y-axis end pixel of the canvas.
    """

    vm.set_canvas_end_pixel_y(int(y))
    return 0.0
    
def fn_fill_canvas(vm, color):
    """
    Fill the canvas with the given color.
    """

    vm.fill_canvas(color)
    return 0.0

def fn_authors(vm):
    """
    Print the authors of this project.
    """

    print('Chappuis Sebastien <3')
    print('Allemann Jonas')
    print('Stouder Xavier')
    return 0.0

# List of native functions with their names as keys
# and a tuple (function, number of arguments) as values
NATIVE_FUNCTIONS = {
    'abs': (fn_abs, 1),
    'pow': (fn_pow, 2),
    'sqrt': (fn_sqrt, 1),
    'min': (fn_min, 2),
    'max': (fn_max, 2),
    'sin': (fn_sin, 1),
    'cos': (fn_cos, 1),
    'tan': (fn_tan, 1),
    'asin': (fn_asin, 1),
    'acos': (fn_acos, 1),
    'atan': (fn_atan, 1),
    'print': (fn_print, 1),
    'color': (fn_color, 1),
    'line': (fn_line, 1),
    'size': (fn_size, 2),
    'canvas_pixel_size': (fn_canvas_pixel_size, 2),
    'canvas_pixel_size_x': (fn_canvas_pixel_size_x, 1),
    'canvas_pixel_size_y': (fn_canvas_pixel_size_y, 1),
    'canvas_start': (fn_canvas_start, 2),
    'canvas_start_x': (fn_canvas_start_x, 1),
    'canvas_start_y': (fn_canvas_start_y, 1),
    'canvas_end': (fn_canvas_end, 2),
    'canvas_end_x': (fn_canvas_end_x, 1),
    'canvas_end_y': (fn_canvas_end_y, 1),
    'next_x': (fn_next_x, 0),
    'next_y': (fn_next_y, 0),
    'newline': (fn_newline, 0),
    'move_x': (fn_next_x, 1),
    'move_y': (fn_next_y, 1),
    'newlines': (fn_newline, 1),
    'cursor': (fn_cursor, 2),
    'cursor_xy': (fn_cursor_xy, 1),
    'cursor_x': (fn_cursor_x, 1),
    'cursor_y': (fn_cursor_y, 1),
    'locator_x': (fn_locator_x, 0),
    'locator_y': (fn_locator_y, 0),
    'authors': (fn_authors, 0),
    'fill_canvas': (fn_fill_canvas, 1),
}


# List of native variables with their names as keys
# and their values as values
NATIVE_VARIABLES = {
    # Math constants
    'PI': math.pi,
    'E': math.e,
    # Colors
    'RED': float(0xFF0000),
    'GREEN': float(0x00FF00),
    'BLUE': float(0x0000FF),
    'YELLOW': float(0xFFFF00),
    'MAGENTA': float(0xFF00FF),
    'CYAN': float(0x00FFFF),
    'WHITE': float(0xFFFFFF),
    'BLACK': float(0x000000),
    'GRAY': float(0x808080),
    # Color: light variants
    'LIGHT_RED': float(0xFF8080),
    'LIGHT_GREEN': float(0x80FF80),
    'LIGHT_BLUE': float(0x8080FF),
    'LIGHT_YELLOW': float(0xFFFF80),
    'LIGHT_MAGENTA': float(0xFF80FF),
    'LIGHT_CYAN': float(0x80FFFF),
    'LIGHT_GRAY': float(0xC0C0C0),
    # Color: dark variants
    'DARK_RED': float(0x800000),
    'DARK_GREEN': float(0x008000),
    'DARK_BLUE': float(0x000080),
    'DARK_YELLOW': float(0x808000),
    'DARK_MAGENTA': float(0x800080),
    'DARK_CYAN': float(0x008080),
    'DARK_GRAY': float(0x404040),
}
