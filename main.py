"""
PixelArc entry point, used with command line to run the choosen functionality.
"""
import os
import sys
from compiler.compiler import compile
from compiler.utils import LangCompilationError
from interpreter.run import run
from interpreter.utils import LangRuntimeError
from enum import Enum

class FileType(Enum):
    """
    Supported file types:
        .txt: Source code
        .pxa: Source code
        .bytecode: Bytecode
        .png: Image
    """

    UNKNOWN = 0
    SOURCE_CODE = 1
    BYTECODE = 2
    IMAGE = 3

def get_file_type(path):
    """
    Get the file type from the file path.
    """

    if path.endswith('.txt') or path.endswith('.pxa'):
        return FileType.SOURCE_CODE
    if path.endswith('.bytecode'):
        return FileType.BYTECODE
    if path.endswith('.png'):
        return FileType.IMAGE
    return FileType.UNKNOWN

def print_help():
    """
    Display the help message.
    """

    print('Usage: python main.py <input_file> [options]')
    print('Options:')
    print('  -o, --output <output_file>      Output file path')
    print('  -g, --graph <graph_file>        AST graph file path (valid extensions: .png, .svg, .pdf)')
    print('  -a, --assembly <assembly_file>  Assembly-like file path')
    print('  -h, --help                      Display this help message')
    print('  -d, --debug                     Enable debug mode (for parser and some messages in the console)')

def beautify_error(prefix, msg):
    """
    Beautify an error message wrapping it into a box.
    """

    msg = '%s: %s' % (prefix, msg)
    bar = '+' + '-' * (len(msg) + 2) + '+'
    return '%s\n| %s |\n%s' % (bar, msg, bar)

def main():
    """
    Entry point.
    """

    # Patch recursion depth problems
    sys.setrecursionlimit(10000)
    
    # Get user arguments (excluding the script name)
    args = sys.argv[1:]

    if len(args) == 0 or args[0] == '-h' or args[0] == '--help':
        print_help()
        return

    current_flag = None

    input_path = args[0]
    input_type = get_file_type(input_path)
    output_path = None
    output_type = FileType.UNKNOWN
    graph_path = None
    assembly_path = None
    debug = False

    for arg in args[1:]:
        if current_flag is None:
            if arg.startswith('-'):
                current_flag = arg
                continue
            else:
                print('Invalid argument: %s' % arg)
                return
        if current_flag == '-o' or current_flag == '--output':
            # Output path
            output_path = arg
            current_flag = None
            continue
        if current_flag == '-g' or current_flag == '--graph':
            # Graph path
            graph_path = arg
            current_flag = None
            continue
        if current_flag == '-a' or current_flag == '--assembly':
            # Assembly path
            assembly_path = arg
            current_flag = None
            continue
        if current_flag == '-d' or current_flag == '--debug':
            # Debug mode
            debug = arg.lower() in ('true', '1')
            current_flag = None
            continue
        print('Invalid argument: %s' % arg)
        return
    

    # Check if the input file has a valid type
    if input_type != FileType.SOURCE_CODE and input_type != FileType.BYTECODE:
        print('Invalid input file type: %s' % input_path)
        print('Supported file types: .txt, .pxa, .bytecode')
        return
    
    if output_path is None:
        print('No output file specified')
        return
    output_type = get_file_type(output_path)

    print(48 * '-')
    print('Input file: %s is %s' % (input_path, input_type))
    print('Output file: %s is %s' % (output_path, output_type))
    print('Graph file: %s' % graph_path)
    print('Assembly file: %s' % assembly_path)
    print(48 * '-')

    # Check if the output file has a valid type
    if output_type != FileType.BYTECODE and output_type != FileType.IMAGE:
        print('Invalid output file type: %s' % output_path)
        print('Supported file types: .bytecode, .png')
        return
    
    # Generate bytecode from source code
    if input_type == FileType.SOURCE_CODE:
        with open(input_path, 'r') as f:
            code = f.read()
        try:
            bytecode = compile(code, graph_path, assembly_path, debug)
        except LangCompilationError as e:
            print(beautify_error('Compilation error', e))
            return
    # Read bytecode from file
    elif input_type == FileType.BYTECODE:
        with open(input_path, 'rb') as f:
            bytecode = f.read()
    
    # Write bytecode to file
    if output_type == FileType.BYTECODE:
        with open(output_path, 'wb') as f:
            f.write(bytecode)
        return # Done
    
    # Run the bytecode to generate an image
    if output_type == FileType.IMAGE:
        try:
            # Step 2: Run the bytecode to generate an image
            img = run(bytecode)
            print(48 * '-')
            print('Image generated successfully !')
            print('Discover your masterpiece at "%s"' % output_path)
            print(48 * '-')
        except LangRuntimeError as e:
            print(beautify_error('Runtime error', e))
            return
        img.save(output_path)
        return # Done
    
    # Should never reach this point
    print('Something went wrong')
    return


if __name__ == '__main__':
    main()
