"""
This module implements the parser for the language.
"""
import os
import imp
import ply.yacc as yacc
import compiler.ast as AST
from compiler.lexer import tokens
from compiler.utils import LangCompilationError

# Operators precedence (highest to lowest)
precedence = (
    ('right', 'SET_OP'),
    ('left', 'OR_OP'),
    ('left', 'AND_OP'),
    ('left', 'EQ_OP', 'NEQ_OP'),
    ('left', 'LT_OP', 'GT_OP', 'LTE_OP', 'GTE_OP'),
    ('left', 'ADD_OP'),
    ('left', 'MUL_OP'),
    ('right', 'UMINUS', 'UNOT'),
)


def p_program_statement(p):
    '''program : statement'''
    p[0] = AST.ProgramNode(p[1])

def p_program_recursive(p):
    '''program : statement program'''
    p[0] = AST.ProgramNode([p[1]] + p[2].children)

def p_statement_declaration(p):
    '''statement : VAR ID SET_OP expression ';' '''
    p[0] = AST.DeclarationNode([AST.VariableDeclNode(p[2]), p[4]])

def p_statement_return(p):
    '''statement : RETURN expression ';' '''
    p[0] = AST.ReturnNode(p[2])

def p_statement_struct(p):
    '''statement : structure'''
    p[0] = p[1]

def p_statement_block(p):
    '''statement : block'''
    p[0] = p[1]

def p_statement_expr(p):
    '''statement : expression ';' '''
    p[0] = AST.DiscardedExprNode(p[1])

def p_block_(p):
    '''block : '{' program '}' '''
    p[0] = p[2]

def p_block_empty(p):
    '''block : '{' '}' '''
    p[0] = AST.ProgramNode([])

def p_structure_conditional(p):
    '''structure : ifblock elifblock elseblock
                 | ifblock elifblock
                 | ifblock elseblock
                 | ifblock'''
    p[0] = AST.ConditionalNode(p[1:])

def p_structure_while(p):
    '''structure : WHILE expression block '''
    p[0] = AST.WhileNode([AST.PreWhileNode(), AST.WhileCondExprNode(p[2]), p[3]])

def p_structure_repeat(p):
    '''structure : REPEAT expression block '''
    p[0] = AST.RepeatNode([AST.RepeatCondExprNode(p[2]), p[3]])

def p_structure_fn(p):
    '''structure : FN ID '(' args ')' block '''
    p[0] = AST.FunctionNode([AST.FunctionDeclNode(p[2]), p[4], p[6]])

def p_structure_fn_noparams(p):
    '''structure : FN ID '(' ')' block '''
    p[0] = AST.FunctionNode([AST.FunctionDeclNode(p[2]), AST.ArgsNode([]), p[5]])

def p_ifblock(p):
    '''ifblock : IF expression block '''
    p[0] = AST.IfNode([AST.IfCondExprNode(p[2]), p[3]])

def p_elifblock_one(p):
    '''elifblock : ELIF expression block '''
    p[0] = AST.ElifGroupNode(AST.ElifNode([AST.ElifCondExprNode(p[2]), p[3]]))

def p_elifblock_recursive(p):
    '''elifblock : ELIF expression block elifblock'''
    p[0] = AST.ElifGroupNode([AST.ElifNode([AST.ElifCondExprNode(p[2]), p[3]])] + p[4].children)

def p_elseblock(p):
    '''elseblock : ELSE block '''
    p[0] = AST.ElseNode(p[2])

def p_expression_assign(p):
    '''expression : ID SET_OP expression'''
    p[0] = AST.AssignNode([AST.VariableAssignNode(p[1]), p[3]])

def p_expression_op(p):
    '''expression : expression ADD_OP expression
                  | expression MUL_OP expression
                  | expression EQ_OP expression
                  | expression NEQ_OP expression
                  | expression LT_OP expression
                  | expression GT_OP expression
                  | expression LTE_OP expression
                  | expression GTE_OP expression
                  | expression AND_OP expression
                  | expression OR_OP expression'''
    p[0] = AST.OpNode(p[2], [p[1], p[3]])

def p_expression_group(p):
    '''expression : "(" expression ")"'''
    p[0] = p[2]

def p_expression_call(p):
    '''expression : ID '(' params ')'
                  | ID ':' params'''
    p[0] = AST.CallNode([AST.FunctionCallNode(p[1]), p[3]])

def p_expression_call_noparams(p):
    '''expression : ID '(' ')' '''
    p[0] = AST.CallNode([AST.FunctionCallNode(p[1]), AST.ParamsNode([])])

def p_expression_uminus(p):
    '''expression : ADD_OP expression %prec UMINUS'''
    p[0] = AST.OpNode(p[1], [p[2]])

def p_expression_unot(p):
    '''expression : NOT_OP expression %prec UNOT'''
    p[0] = AST.OpNode(p[1], [p[2]])

def p_expression_id(p):
    '''expression : ID'''
    p[0] = AST.VariableReadNode(p[1])

def p_expression_num(p):
    '''expression : NUMBER
                  | COLOR6
                  | COLOR3
                  | BOOL'''
    p[0] = AST.NumberNode(p[1])


def p_args_arg(p):
    '''args : ID'''
    p[0] = AST.ArgsNode(AST.ArgumentNode(p[1]))

def p_args_recursive(p):
    '''args : ID ',' args'''
    p[0] = AST.ArgsNode([AST.ArgumentNode(p[1])] + p[3].children)


def p_params_param(p):
    '''params : expression'''
    p[0] = AST.ParamsNode(p[1])

def p_params_recursive(p):
    '''params : expression ',' params'''
    p[0] = AST.ParamsNode([p[1]] + p[3].children)


def p_error(p):
    global error_message
    if p is None:
        error_message = 'Unexpected token'
    else:
        error_message = 'Line %d, unexpected token %s' % (p.lineno, p.type)
        parser.errok()


outputdir = 'out/parser/'
parser = yacc.yacc(outputdir=outputdir, debug=False, write_tables=False)
error_message = None


def parse(code, debug=False):
    """
    Parse the given code and return the AST.
    """
    global error_message
    global parser
    
    # Debug
    if debug:
        if not os.path.exists(outputdir):
            os.makedirs(outputdir)
        parser = yacc.yacc(debug=True, outputdir=outputdir, write_tables=False)
    # End debug
    
    error_message = None
    
    ast = parser.parse(code)
    if error_message:
        raise LangCompilationError(error_message)
    return ast
