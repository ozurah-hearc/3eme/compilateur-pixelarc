"""
This module implements the threader
"""
import compiler.ast as AST
from common.decorators import add_to_class

@add_to_class(AST.Node)
def thread(self, last_node):
    """
    Thread function for Node
    """

    for c in self.children:
        last_node = c.thread(last_node)
    last_node.addNext(self)
    return self

@add_to_class(AST.CallNode)
def thread(self, last_node):
    """
    Thread function for CallNode
    """

    for c in self.children:
        last_node = c.thread(last_node)
    last_node.addNext(self)
    return self

@add_to_class(AST.ArgsNode)
def thread(self, last_node):
    """
    Thread function for ArgsNode
    """

    # Pass the args node first
    last_node.addNext(self) 
    last_node = self
    for c in self.children:
        last_node = c.thread(last_node)
    return last_node


def thread(tree):
    entry = AST.EntryNode()
    tree.thread(entry)
    return entry
