# coding: latin-1

''' Petit module utilitaire pour la construction, la manipulation et la 
représentation d'arbres syntaxiques abstraits.

Sûrement plein de bugs et autres surprises. À prendre comme un 
"work in progress"...
Notamment, l'utilisation de pydot pour représenter un arbre syntaxique cousu
est une utilisation un peu "limite" de graphviz. Ça marche, mais le layout n'est
pas toujours optimal...
'''

import pydot

class Node:
    count = 0
    type = 'Node (unspecified)'
    shape = 'ellipse'
    def __init__(self,children=None):
        self.ID = str(Node.count)
        Node.count+=1
        if not children: self.children = []
        elif hasattr(children,'__len__'):
            self.children = children
        else:
            self.children = [children]
        self.next = []

    def addNext(self,next):
        self.next.append(next)

    def asciitree(self, prefix=''):
        result = "%s%s\n" % (prefix, repr(self))
        prefix += '|  '
        for c in self.children:
            if not isinstance(c,Node):
                result += "%s*** Error: Child of type %r: %r\n" % (prefix,type(c),c)
                continue
            result += c.asciitree(prefix)
        return result
    
    def __str__(self):
        return self.asciitree()
    
    def __repr__(self):
        return self.type
    
    def makegraphicaltree(self, dot=None, edgeLabels=True):
            if not dot: dot = pydot.Dot()
            dot.add_node(pydot.Node(self.ID,label=repr(self), shape=self.shape))
            label = edgeLabels and len(self.children)-1
            for i, c in enumerate(self.children):
                c.makegraphicaltree(dot, edgeLabels)
                edge = pydot.Edge(self.ID,c.ID)
                if label:
                    edge.set_label(str(i))
                dot.add_edge(edge)
                #Workaround for a bug in pydot 1.0.2 on Windows:
                #dot.set_graphviz_executables({'dot': r'C:\Program Files\Graphviz2.38\bin\dot.exe'})
            return dot
        
    def threadTree(self, graph, seen = None, col=0):
            colors = ('red', 'darkgreen', 'blue', 'orange', 'magenta', 'skyblue')
            if not seen: seen = []
            if self in seen: return
            seen.append(self)
            new = not graph.get_node(self.ID)
            if new:
                graphnode = pydot.Node(self.ID,label=repr(self), shape=self.shape)
                graphnode.set_style('dotted')
                graph.add_node(graphnode)
            label = len(self.next)-1
            for i,c in enumerate(self.next):
                if not c: return
                col = (col + 1) % len(colors)
                #col=0 # FRT pour tout afficher en rouge
                color = colors[col]                
                c.threadTree(graph, seen, col)
                edge = pydot.Edge(self.ID,c.ID)
                edge.set_color(color)
                edge.set_arrowsize('.5')
                # Les arrêtes correspondant aux coutures ne sont pas prises en compte
                # pour le layout du graphe. Ceci permet de garder l'arbre dans sa représentation
                # "standard", mais peut provoquer des surprises pour le trajet parfois un peu
                # tarabiscoté des coutures...
                # En commantant cette ligne, le layout sera bien meilleur, mais l'arbre nettement
                # moins reconnaissable.
                edge.set_constraint('false') 
                if label:
                    edge.set_taillabel(str(i))
                    edge.set_labelfontcolor(color)
                graph.add_edge(edge)
            return graph    

class ProgramNode(Node):
    type = 'Program'

class TokenNode(Node):
    type = 'generic token'
    def __init__(self, tok):
        Node.__init__(self)
        self.tok = tok
    def __repr__(self):
        return repr(self.tok)

class VariableDeclNode(TokenNode):
    type = 'variable declaration token'

class VariableAssignNode(TokenNode):
    type = 'variable assignment token'

class VariableReadNode(TokenNode):
    type = 'variable read token'

class NumberNode(TokenNode):
    type = 'number token'

class FunctionDeclNode(TokenNode):
    type = 'function declaration token'

class FunctionCallNode(TokenNode):
    type = 'function call token'

class ArgumentNode(TokenNode):
    type = 'argument token'

class OpNode(Node):
    def __init__(self, op, children):
        Node.__init__(self,children)
        self.op = op
        try:
            self.nbargs = len(children)
        except AttributeError:
            self.nbargs = 1
        
    def __repr__(self):
        return "%s (%s)" % (self.op, self.nbargs)

class DiscardedExprNode(Node):
    type = 'discarded expr'

class DeclarationNode(Node):
    type = 'var decl'

class AssignNode(Node):
    type = 'set'

class ConditionalNode(Node):
    type = 'cond'

class IfNode(Node):
    type = 'if'

class ElifGroupNode(Node):
    type = 'elif group'

class ElifNode(Node):
    type = 'elif'

class ElseNode(Node):
    type = 'else'

class IfCondExprNode(Node):
    type = 'if cond expr'

class ElifCondExprNode(Node):
    type = 'elif cond expr'

class WhileNode(Node):
    type = 'while'

class PreWhileNode(Node):
    type = 'pre while'

class WhileCondExprNode(Node):
    type = 'while cond expr'

class RepeatNode(Node):
    type = 'repeat'

class RepeatCondExprNode(Node):
    type = 'repeat cond expr'

class FunctionNode(Node):
    type = 'fn decl'

class CallNode(Node):
    type = 'call'

class ReturnNode(Node):
    type = 'return'

class ArgsNode(Node):
    type = 'args'
    def __repr__(self):
        return '%s (%s)' % (self.type, len(self.children))

class ParamsNode(Node):
    type = 'params'
    def __repr__(self):
        return '%s (%s)' % (self.type, len(self.children))

class EntryNode(Node):
    type = 'ENTRY'
    def __init__(self):
        Node.__init__(self, None)
