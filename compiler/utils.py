"""
This module contains utility classes and functions for the compiler.
"""

class Label:
    """
    A label is a reference to a line of code.
    """

    def __init__(self, name):
        """
        Create a new label with the given name.
        """
        self.name = name
    
    def __str__(self):
        """
        Return the name of the label.
        """
        return self.name


class Instruction:
    """
    An instruction is a line of code that will be compiled to bytecode.
    """

    def __init__(self, opcode, *args):
        """
        Create a new instruction with the given opcode and arguments.
        """
        self.opcode = opcode
        self.args = args

    def __str__(self):
        """
        Return a string representation of the instruction.
        """
        arrow = (10 - len(str(self.opcode))) * '-' + '>'
        args = map(str, self.args)
        args = ', '.join(args)
        if args:
            return f'  {self.opcode} {arrow} {args}'
        else:
            return f'  {self.opcode}'


class LangCompilationError(Exception):
    """
    An error that occurs during compilation.
    """
    pass
