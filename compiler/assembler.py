"""
Assembler for the language
"""

import compiler.ast as AST
from compiler.utils import *
from common.natives import NATIVE_FUNCTIONS, NATIVE_VARIABLES
from common.opcodes import Opcode
from common.decorators import add_to_class

class Context:
    """
    A class to store the ctx of the compiler,
    used to keep track of which variables are declared,
    in which function we are, etc.
    """

    def __init__(self):
        """
        Initialize the ctx.
        """
        self.global_vars = {} # Dict of declared global variables and if they are used or not
        self.local_vars = {} # Dict of declared local variables and if they are used or not
        self.functions = {} # Dict of declared functions and their instructions
        self.conditional_indexes = [] # List of list [cond_id, cond_index] to keep track of the indexes of the conditional jumps
        self.counter = 0 # Counter used to generate unique label names
        self.current_fn_name = None # Name of the current function or None if in global scope
        self.functions['@global'] = [] # List of instructions for the global scope
        self.warnings = [] # List of warnings

    def is_in_global_scope(self):
        """
        Return True if we are in the global scope, False otherwise.
        """
        return self.current_fn_name is None

    def is_in_local_scope(self):
        """
        Return True if we are in a local scope, False otherwise.
        """
        return not self.is_in_global_scope()

# Todo no magic on default values
meta = {
    'size': (256,256),
    'cursor': (0,0),
}

def assemble(entry, assembly_path=None):
    """
    Generate assembly-like instructions from the AST.
    """

    node = entry
    ctx = Context()
    # For each node in the tree
    while True:
        # Name of the current function or '@global' if in global scope
        label_name = ctx.current_fn_name if ctx.is_in_local_scope() else '@global'
        if label_name not in ctx.functions:
            ctx.functions[label_name] = [] # Empty list of instructions
        instructions = ctx.functions[label_name]
        lines = node.get_lines(ctx)

        if not isinstance(lines, list):
            lines = [lines]
        for line in lines:
            instructions.append(line)
        
        if not node.next:
            break

        node = node.next[0]
    
    ctx.functions['@global'].append(Instruction(Opcode.END))
    
    assembly_like = []
    for fn_name, instructions in ctx.functions.items():
        assembly_like.append(Label(fn_name))
        for line in instructions:
            assembly_like.append(line)

    if assembly_path is not None:
        assembly_output = []
        for line in assembly_like:
            if isinstance(line, Instruction):
                opcode = line.opcode
                args = line.args
                arrow = (10 - len(str(opcode))) * '-' + '>'
                args = map(str, args)
                args = ', '.join(args)
                if args:
                    assembly_output.append('  %s %s %s' % (opcode, arrow, args))
                else:
                    assembly_output.append('  %s' % opcode)
            elif isinstance(line, Label):
                label = line.name
                assembly_output.append('%s:' % label)
            else:
                raise LangCompilationError('Unknown line type: %s' % line)
        with open(assembly_path, 'w') as f:
            f.write('\n'.join(assembly_output))
    
    # Check for unused global variables
    for var_name, is_used in ctx.global_vars.items():
        if not is_used:
            ctx.warnings.append('Unused global variable %s' % var_name)
    
    # Display warnings
    for warning in ctx.warnings:
        print('Warning: %s' % warning)

    return assembly_like


def decl_gvar(ctx, name, may_be_unused=False):
    """
    Declare a global variable.
    """

    if name in ctx.global_vars:
        raise LangCompilationError('Global variable %s already declared' % name)
    ctx.global_vars[name] = may_be_unused
    return Instruction(Opcode.GVAR_DECL, name)

def decl_lvar(ctx, name, may_be_unused=False):
    """
    Declare a local variable.
    """

    if name in ctx.local_vars:
        raise LangCompilationError('Local variable %s already declared' % name)
    ctx.local_vars[name] = may_be_unused
    return Instruction(Opcode.LVAR_DECL, name)


@add_to_class(AST.Node)
def get_lines(self, ctx):
    """
    Return assembly-like instructions for Node.
    """

    return []

@add_to_class(AST.TokenNode)
def get_lines(self, ctx):
    """
    Return assembly-like instructions for TokenNode.
    """

    raise NotImplementedError('get_lines() is not implemented for token node of type ' + str(type(self)))

@add_to_class(AST.EntryNode)
def get_lines(self, ctx):
    """
    Return assembly-like instructions for EntryNode.
    """

    lines = []
    # For each native variable, declare it with the correct value
    for var_name, value in NATIVE_VARIABLES.items():
        lines.append(Instruction(Opcode.ACC_PUSH, value))
        lines.append(decl_gvar(ctx, var_name, may_be_unused=True))
    return lines

@add_to_class(AST.DeclarationNode)
def get_lines(self, ctx):
    """
    Return assembly-like instructions for DeclarationNode.
    """

    var_name = self.children[0].tok
    if ctx.is_in_local_scope():
        return decl_lvar(ctx, var_name)
    return decl_gvar(ctx, var_name)

@add_to_class(AST.VariableDeclNode)
def get_lines(self, ctx):
    """
    Return assembly-like instructions for VariableDeclNode.
    """

    return []

@add_to_class(AST.AssignNode)
def get_lines(self, ctx):
    """
    Return assembly-like instructions for AssignNode.
    """

    var_name = self.children[0].tok
    if var_name in ctx.local_vars:
        ctx.local_vars[var_name] = True # Mark the variable as used
        return Instruction(Opcode.LVAR_SET, var_name)
    if var_name in ctx.global_vars:
        ctx.global_vars[var_name] = True # Mark the variable as used
        return Instruction(Opcode.GVAR_SET, var_name)
    raise LangCompilationError('Cannot assign to undeclared variable %s' % var_name)

@add_to_class(AST.VariableAssignNode)
def get_lines(self, ctx):
    """
    Return assembly-like instructions for VariableAssignNode.
    """
    return []

@add_to_class(AST.IfNode)
def get_lines(self, ctx):
    """
    Return assembly-like instructions for IfNode.
    """

    cond_info = ctx.conditional_indexes[-1]
    end_label = '@if%04d_end' % cond_info[0]
    next_label = '@if%04d_i%02d' % (cond_info[0], cond_info[1])
    return [
        Instruction(Opcode.GOTO, Label(end_label)),
        Label(next_label),
    ]

@add_to_class(AST.ElifNode)
def get_lines(self, ctx):
    """
    Return assembly-like instructions for ElifNode.
    """

    cond_info = ctx.conditional_indexes[-1]
    end_label = '@if%04d_end' % cond_info[0]
    next_label = '@if%04d_i%02d' % (cond_info[0], cond_info[1])
    return [
        Instruction(Opcode.GOTO, Label(end_label)),
        Label(next_label),
    ]

@add_to_class(AST.ElseNode)
def get_lines(self, ctx):
    """
    Return assembly-like instructions for ElseNode.
    """

    cond_info = ctx.conditional_indexes[-1]
    end_label = '@if%04d_end' % cond_info[0]
    next_label = '@if%04d_i%02d' % (cond_info[0], cond_info[1])
    return [
        Instruction(Opcode.GOTO, Label(end_label)),
    ]

@add_to_class(AST.ConditionalNode)
def get_lines(self, ctx):
    """
    Return assembly-like instructions for ConditionalNode.
    """

    # End of the conditional block
    cond_info = ctx.conditional_indexes.pop()
    end_label = '@if%04d_end' % cond_info[0]
    return Label(end_label)

@add_to_class(AST.IfCondExprNode)
def get_lines(self, ctx):
    """
    Return assembly-like instructions for IfCondExprNode.
    """

    cond_info = [ctx.counter, 0]
    ctx.conditional_indexes.append(cond_info)
    ctx.counter += 1
    next_label = '@if%04d_i%02d' % (cond_info[0], cond_info[1])
    return Instruction(Opcode.ELSE_GO, Label(next_label))

@add_to_class(AST.ElifCondExprNode)
def get_lines(self, ctx):
    """
    Return assembly-like instructions for ElifCondExprNode.
    """

    cond_info = ctx.conditional_indexes[-1]
    cond_info[1] += 1 # Increment the index of the conditional jump
    next_label = '@if%04d_i%02d' % tuple(cond_info)
    return Instruction(Opcode.ELSE_GO, Label(next_label))

@add_to_class(AST.PreWhileNode)
def get_lines(self, ctx):
    """
    Return assembly-like instructions for PreWhileNode.
    """

    cond_info = [ctx.counter, 0]
    ctx.conditional_indexes.append(cond_info)
    ctx.counter += 1
    label = '@while%04d_start' % cond_info[0]
    return Label(label)

@add_to_class(AST.WhileCondExprNode)
def get_lines(self, ctx):
    """
    Return assembly-like instructions for WhileCondExprNode.
    """

    cond_info = ctx.conditional_indexes[-1]
    end_label = '@while%04d_end' % cond_info[0]
    return Instruction(Opcode.ELSE_GO, Label(end_label))

@add_to_class(AST.WhileNode)
def get_lines(self, ctx):
    """
    Return assembly-like instructions for WhileNode.
    """

    cond_info = ctx.conditional_indexes.pop()
    start_label = '@while%04d_start' % cond_info[0]
    end_label = '@while%04d_end' % cond_info[0]
    return [
        Instruction(Opcode.GOTO, Label(start_label)),
        Label(end_label),
    ]

@add_to_class(AST.RepeatCondExprNode)
def get_lines(self, ctx):
    """
    Return assembly-like instructions for RepeatCondExprNode.
    """

    cond_info = [ctx.counter, 0]
    ctx.conditional_indexes.append(cond_info)
    ctx.counter += 1
    start_label = '@repeat%04d_start' % cond_info[0]
    end_label = '@repeat%04d_end' % cond_info[0]
    counter_var = '@counter%04d' % cond_info[0]
    # Simulate a while loop
    return [
        Instruction(Opcode.LVAR_DECL, counter_var),
        Label(start_label),
        Instruction(Opcode.LVAR_GET, counter_var),
        Instruction(Opcode.ACC_PUSH, 0.0),
        Instruction(Opcode.GT),
        Instruction(Opcode.ELSE_GO, Label(end_label)),
    ]

@add_to_class(AST.RepeatNode)
def get_lines(self, ctx):
    """
    Return assembly-like instructions for RepeatNode.
    """

    cond_info = ctx.conditional_indexes.pop()
    start_label = '@repeat%04d_start' % cond_info[0]
    end_label = '@repeat%04d_end' % cond_info[0]
    return [
        Instruction(Opcode.LVAR_GET, '@counter%04d' % cond_info[0]),
        Instruction(Opcode.ACC_PUSH, 1.0),
        Instruction(Opcode.SUB),
        Instruction(Opcode.LVAR_SET, '@counter%04d' % cond_info[0]),
        Instruction(Opcode.ACC_POP),
        Instruction(Opcode.GOTO, Label(start_label)),
        Label(end_label),
    ]

@add_to_class(AST.OpNode)
def get_lines(self, ctx):
    """
    Return assembly-like instructions for OpNode.
    """

    # If it's an unary operator
    if len(self.children) == 1:
        if self.op == '+':
            return [] # Unary plus does nothing
        elif self.op == '-':
            return Instruction(Opcode.NEG)
        elif self.op == '!':
            return Instruction(Opcode.NOT)
        raise LangCompilationError('Unknown unary operator %s' % self.op)
    # If it's a binary operator
    if len(self.children) == 2:
        if   self.op == '+':  return Instruction(Opcode.ADD)
        elif self.op == '-':  return Instruction(Opcode.SUB)
        elif self.op == '*':  return Instruction(Opcode.MUL)
        elif self.op == '/':  return Instruction(Opcode.DIV)
        elif self.op == '%':  return Instruction(Opcode.MOD)
        elif self.op == '==': return Instruction(Opcode.EQ)
        elif self.op == '!=': return Instruction(Opcode.NEQ)
        elif self.op == '<':  return Instruction(Opcode.LT)
        elif self.op == '>':  return Instruction(Opcode.GT)
        elif self.op == '<=': return Instruction(Opcode.LTE)
        elif self.op == '>=': return Instruction(Opcode.GTE)
        elif self.op == '&&': return Instruction(Opcode.AND)
        elif self.op == '||': return Instruction(Opcode.OR)
        raise LangCompilationError('Unknown binary operator %s' % self.op)
    raise LangCompilationError('Invalid number of children for operator node: %d' % len(self.children))

@add_to_class(AST.DiscardedExprNode)
def get_lines(self, ctx):
    """
    Return assembly-like instructions for DiscardedExprNode.
    """

    return Instruction(Opcode.ACC_POP)

@add_to_class(AST.CallNode)
def get_lines(self, ctx):
    """
    Return assembly-like instructions for CallNode.
    """

    fn_name = self.children[0].tok
    nb_params = len(self.children[1].children)

    # Handle semantic errors for native functions
    if fn_name in NATIVE_FUNCTIONS:
        return Instruction(Opcode.NAT_CALL, fn_name, nb_params)
    return Instruction(Opcode.CALL, Label(fn_name), nb_params)

@add_to_class(AST.FunctionNode)
def get_lines(self, ctx):
    """
    Return assembly-like instructions for CallNode.
    """

    # Check for unused local variables
    for var_name, is_used in ctx.local_vars.items():
        if not is_used:
            ctx.warnings.append('Unused local variable %s in function %s' % (var_name, ctx.current_fn_name))
    ctx.local_vars.clear()
    # End of function definition, go back to global scope
    ctx.current_fn_name = None
    return [
        Instruction(Opcode.ACC_PUSH, 0.0),
        Instruction(Opcode.RETURN),
    ]

@add_to_class(AST.ArgumentNode)
def get_lines(self, ctx):
    """
    Return assembly-like instructions for ArgumentNode.
    """

    var_name = self.tok
    ctx.local_vars[var_name] = False # Not used yet
    return Instruction(Opcode.LVAR_DECL, var_name)

@add_to_class(AST.ArgsNode)
def get_lines(self, ctx):
    """
    Return assembly-like instructions for ArgsNode.
    """

    nb_args = len(self.children)
    return [
        Instruction(Opcode.ACC_PUSH, float(nb_args)),
        Instruction(Opcode.FN_CHECK),
    ]

@add_to_class(AST.FunctionDeclNode)
def get_lines(self, ctx):
    """
    Return assembly-like instructions for FunctionDeclNode.
    """

    fn_name = self.tok
    if ctx.is_in_local_scope():
        parent_fn_name = ctx.current_fn_name
        raise LangCompilationError('Cannot declare the function %s inside the function %s' % (fn_name, parent_fn_name))
    if fn_name in ctx.functions or fn_name in NATIVE_FUNCTIONS:
        raise LangCompilationError('Function %s already declared' % fn_name)
    if len(ctx.conditional_indexes) > 0:
        raise LangCompilationError('Cannot declare the function %s inside a conditional block' % fn_name)
    # Change the chunk of code
    ctx.current_fn_name = fn_name
    return []

@add_to_class(AST.FunctionCallNode)
def get_lines(self, ctx):
    """
    Return assembly-like instructions for FunctionCallNode.
    """

    fn_name = self.tok
    if fn_name not in ctx.functions and fn_name not in NATIVE_FUNCTIONS:
        raise LangCompilationError('Cannot call undeclared function %s' % fn_name)
    return []

@add_to_class(AST.ReturnNode)
def get_lines(self, ctx):
    """
    Return assembly-like instructions for ReturnNode.
    """

    return Instruction(Opcode.RETURN)

@add_to_class(AST.VariableReadNode)
def get_lines(self, ctx):
    """
    Return assembly-like instructions for VariableReadNode.
    """

    var_name = self.tok
    if var_name in ctx.local_vars:
        ctx.local_vars[var_name] = True # Mark as used
        return Instruction(Opcode.LVAR_GET, var_name)
    if var_name in ctx.global_vars:
        ctx.global_vars[var_name] = True # Mark as used
        return Instruction(Opcode.GVAR_GET, var_name)
    raise LangCompilationError('Cannot use undeclared variable %s' % var_name)

@add_to_class(AST.NumberNode)
def get_lines(self, ctx):
    """
    Return assembly-like instructions for NumberNode.
    """

    number = self.tok
    return Instruction(Opcode.ACC_PUSH, number)
