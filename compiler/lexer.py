"""
Lexer for the language.
"""

import sys
import ply.lex as ply_lex

reserved_words = (
    'var',
    'if',
    'elif',
    'else',
    'while',
    'repeat',
    'fn',
    'return',
)

tokens = (
    'NUMBER',
    'COLOR6',
    'COLOR3',
    'BOOL',
    'ADD_OP',
    'MUL_OP',
    'SET_OP',
    'EQ_OP',
    'NEQ_OP',
    'LT_OP',
    'GT_OP',
    'LTE_OP',
    'GTE_OP',
    'AND_OP',
    'OR_OP',
    'NOT_OP',
    'ID',
) + tuple(map(lambda s: s.upper(), reserved_words))

literals = '(){}:;,='

t_ADD_OP = r'\+|-'
t_MUL_OP = r'\*|/|%'
t_SET_OP = r'='
t_EQ_OP = r'=='
t_NEQ_OP = r'!='
t_LT_OP = r'<'
t_GT_OP = r'>'
t_LTE_OP = r'<='
t_GTE_OP = r'>='
t_AND_OP = r'&&'
t_OR_OP = r'\|\|'
t_NOT_OP = r'!'

t_ignore = ' \t'


def t_NUMBER(t):
    r'[0-9]+\.?[0-9]*'
    t.value = float(t.value)
    return t

def t_COLOR6(t):
    r'\#[0-9a-fA-F]{6}'
    t.value = t.value[1:]
    # Hex 'RRGGBB' to float
    t.value = float(int(t.value, 16))
    return t

def t_COLOR3(t):
    r'\#[0-9a-fA-F]{3}'
    t.value = t.value[1:]
    # Hex 'RGB' to 'RRGGBB'
    t.value = 2*t.value[0] + 2*t.value[1] + 2*t.value[2]
    # Hex 'RRGGBB' to float
    t.value = float(int(t.value, 16))
    return t

def t_BOOL(t):
    r'true|false'
    t.value = float(t.value == 'true')
    return t

def t_ID(t):
    r'[A-z_][A-z0-9_]*'
    if t.value in reserved_words:
        t.type = t.value.upper()
    return t

def t_COMMENT(t):
    r'//.*'
    pass
    # No return value. Token discarded


def t_newline(t):
    r'\n+'
    t.lexer.lineno += len(t.value)

def t_error(t):
    #print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)


ply_lex.lex()

def lex(code):
    """
    Lexes the given code.
    """

    ply_lex.input(code)
    while True:
        tok = ply_lex.token()
        if not tok:
            break
        #print('Line %d: %s(%s)' % (tok.lineno, tok.type, tok.value))
