"""
This module is the main interface to the compiler.
"""

from compiler.parser import parse
from compiler.threader import thread
from compiler.assembler import assemble
from compiler.builder import build
from compiler.utils import LangCompilationError

def compile(code, graph_path=None, assembly_path=None, debug=False):
    """
    Execute the compilation process.
    Input: source code
    Output: bytecode
    """
    ast = parse(code, debug)
    entry_node = thread(ast)
    if graph_path is not None:
        graph = ast.makegraphicaltree()
        entry_node.threadTree(graph)

        if graph_path.endswith('.png'):
            graph.write_png(graph_path)
        elif graph_path.endswith('.svg'):
            graph.write_svg(graph_path)
        elif graph_path.endswith('.pdf'):
            graph.write_pdf(graph_path)
        else:
            raise LangCompilationError('Invalid graph file type: %s' % graph_path)
        print("AST file correctly saved to", graph_path)
    assembly_like = assemble(entry_node, assembly_path)
    bytecode = build(assembly_like, debug)
    return bytecode
