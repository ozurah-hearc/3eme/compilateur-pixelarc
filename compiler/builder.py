"""
This module contains the bytecode builder.
"""

from common.opcodes import Opcode
from compiler.utils import *
import struct

def float_to_bytes(f):
    """
    Convert a float to a 4-byte little-endian byte array.
    """
    return struct.pack('f', f)

def int_to_bytes(i):
    """
    Convert an integer to a 4-byte little-endian byte array.
    """
    return i.to_bytes(4, 'little')


def build(assembly_like, debug=False):
    """
    Generate bytecode from an assembly-like list of instructions/labels.
    """

    ADDR_BYTE_LEN = 4
    OPCODE_BYTE_LEN = 1
    bytecode = bytearray()
    # Dict of labels and their position in the bytecode
    labels_to_addrs = {}
    # List of tuples (addr_src, addr_dest)
    # where addr_src is the position of the instruction that needs to be patched
    # and addr_dest is the position of the label to jump to
    addrs_to_patch = []

    # First pass: generate bytecode and store labels
    for line in assembly_like:
        if isinstance(line, Label):
            labels_to_addrs[line.name] = len(bytecode)
        elif isinstance(line, Instruction):
            opcode = line.opcode
            args = line.args
            bytecode.append(opcode.value)
            for arg in args:
                if isinstance(arg, Label):
                    addrs_to_patch.append((len(bytecode), arg.name))
                    bytecode += ADDR_BYTE_LEN * b'\0' # Will be patched in the second pass
                elif isinstance(arg, int):
                    bytecode += int_to_bytes(arg)
                elif isinstance(arg, float):
                    bytecode += float_to_bytes(arg)
                elif isinstance(arg, str):
                    bytecode += arg.encode('utf-8') + b'\0' # Null-terminated string
                else:
                    raise Exception('Unknown argument type: %s' % type(arg))
    
    # Second pass: patch labels
    for addr_src, label_name in addrs_to_patch:
        addr_dest = labels_to_addrs[label_name]
        bytecode[addr_src:addr_src+ADDR_BYTE_LEN] = addr_dest.to_bytes(ADDR_BYTE_LEN, 'little')
        if debug:
            print('Patched %s (goto %s) with %s' % (addr_src, label_name, addr_dest))
    
    return bytecode
