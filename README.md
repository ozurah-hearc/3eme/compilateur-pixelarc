# PixelARC

PixelARC permet la génération d'image de type pixel art à partir d'une image source, il est également possible de générer un bytecode intermédiaire.

Le projet se trouve sur GitLab, à cette adresse : https://gitlab-etu.ing.he-arc.ch/isc/2022-23/niveau-3/3250_1_compilateur/projet-isc3/grp17_pixelarc_allemann-chappuis-stouder 

# Installation

## Prérequis

**Langage**
- Python 3.11.1

**Librairies**
- Numpy 1.24.1
- Pillow 9.4.0
- Ply 3.11
- PyDot 1.4.2

## Obtention du projet
Clonnez le projet depuis GitLab :
-	SSH :
`git@gitlab-etu.ing.he-arc.ch:isc/2022-23/niveau-3/3250_1_compilateur/projet-isc3/grp17_pixelarc_allemann-chappuis-stouder.git`
-	HTTPS : 
`https://gitlab-etu.ing.he-arc.ch/isc/2022-23/niveau-3/3250_1_compilateur/projet-isc3/grp17_pixelarc_allemann-chappuis-stouder.git`

## Lancez votre premier programme PixelARC

Commencez par ouvrir un terminal.
```bash
# Accédez au dossier du projet
cd [cheminDuProjet]/

# Créez un dossier pour vos sorties
mkdir out

# Exécutez le programme avec le fichier « examples/hello.pxa »
python main.py ./examples/hello.pxa --output ./out/hello.png
```

Allez contempler l’image que vous venez de générer (dans « ./out/ »).<br>
Vous devriez voir une magnifique ligne alternant le rouge, vert et bleu. La taille de l’image est de 15x1.<br>
![hello.png que vous devriez obtenir](./images/hello.png)


# Architecture du projet

Le projet est composé de deux grandes parties :
- Le compilateur PixelARC (dossier `compiler/`)
- L'interpréteur PixelARC (dossier `interpreter/`)

D'autres dossiers sont également présents dans le projet :
- `examples/` : contient des exemples de code PixelARC
- `common/` : contient des fichiers communs aux deux principales parties du projet

## Utilisation en ligne de commande

```bash
python main.py <fichier_entrée> [options]
```

Options possibles :
- `-o, --output <output_file>`Fichier de sortie (obligatoire)`
- `-g, --graph <graph_file>` Graph de l’AST cousu (facultative)
- `-a, --assembly <assembly_file>` Représentation de l’assembly-like (facultative)`
- `-h, --help` Affiche l’aide
- `-d, --debug true` Génère la grammaire du langage dans le dossier « out/ »

Extensions possible :
- Fichier d'entrée :
  - `*.pxa`, `*.txt` (code source)
  - `*.bytecode` (bytecode)
- Fichier de sortie :
  - `*.bytecode` (bytecode)
  - `*.png` (image)
- Fichier du graphe :
  - `*.png` (image)
  - `*.svg` (image vectorielle)	
  - `*.pdf` (document PDF, avec l'image vectorielle)

Plus d'informations sur le langage PixelARC et son implémentation sont disponibles dans le rapport de projet.
