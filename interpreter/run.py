"""
PixelArc bytecode interpreter
"""

import struct
from enum import Enum

import numpy as np
from PIL import Image
from common.decorators import add_to_class, add_to_dict
from common.natives import NATIVE_FUNCTIONS
from common.opcodes import Opcode
from interpreter.utils import LangRuntimeError

class VM:
    """
    The virtual machine stores the state of the running program,
    like global/local variables, the current instruction pointer, etc.
    """

    def __init__(self, bytecode):
        """
        Create a new virtual machine with the given bytecode
        """

        self.bytecode = bytecode
        self.global_vars = {}
        self.local_vars_stack = [{}] # List of dicts
        self.acc = []
        self.addr = 0

        self.cursor_x = 0
        self.cursor_y = 0
        self.color = (0, 0, 0)
        self.canvas_color = (0, 0, 0)
        
        self.pixel_size_x = 1
        self.pixel_size_y = 1
        
        self.start_pixel_x = 0
        self.start_pixel_y = 0
        
        # Todo no magic
        self.end_pixel_x = 256
        self.end_pixel_y = 256

        # Create a PIL image representing a 2D array of pixels
        self.image = Image.new('RGB', (256, 256), color=self.canvas_color)

    def check_cursor_out_of_image(self):
        """
        Check if the cursor is out of the image
        """

        if self.cursor_x >= self.image.width or self.cursor_y >= self.image.height:
            raise LangRuntimeError("Cursor out of image")

    def set_color(self, color):    
        """
        Set current color variable
        """

        self.color = self.numbercolor_to_rgb(color)

    def set_canvas_color(self, color):
        """
        Set canvas color variable
        """

        self.canvas_color = self.numbercolor_to_rgb(color)

    def numbercolor_to_rgb(self, color):
        """
        Convert an integer color to RGB
        """
        color = int(color)

        r = (color >> 16) & 0xFF
        g = (color >> 8) & 0xFF
        b = color & 0xFF
        return (r, g, b)

    def move_cursor_forward(self, nb=1):
        """
        Move the cursor to the N-th next pixel along the x-axis,
        wrapping around to the next row if necessary
        """
        self.cursor_x += nb

        offsetX = self.cursor_x % self.image.width
        offsetY = self.cursor_x // self.image.width
        
        self.cursor_x = offsetX
        self.cursor_y = self.cursor_y + offsetY

    def move_cursor_downward(self, nb=1):
        """
        Move the cursor to the N-th next pixel along the y-axis,
        wrapping around to the next column if necessary
        """

        self.cursor_y += nb
        
        offsetX = self.cursor_y // self.image.height
        offsetY = self.cursor_y % self.image.height
        
        self.cursor_x = self.cursor_x + offsetX
        self.cursor_y = offsetY

    def move_cursor_newline(self, nb=1):
        """
        Move the cursor to the N-th next pixel along the x-axis,
        wrapping around to the next row if necessary
        """

        self.cursor_x = 0
        self.cursor_y += nb
        if self.cursor_y >= self.image.height:
            self.cursor_y = 0
            
    def move_cursor_at(self, x, y):
        """
        Move the cursor to the given coordinates
        """

        self.cursor_x = x
        self.cursor_y = y

        
    def get_cursor_x(self):
        """
        Get the current cursor x coordinate
        """

        return self.cursor_x
    
    def get_cursor_y(self):
        """
        Get the current cursor y coordinate
        """

        return self.cursor_y
    
    def set_canvas_size(self, width, height):
        """
        Set the size of the canvas
        """

        width = int(width)
        height = int(height)
        min_width = min(self.image.width, width)
        min_height = min(self.image.height, height)
        
        self.start_pixel_x = 0
        self.start_pixel_y = 0
        self.end_pixel_x = width
        self.end_pixel_y = height
        
        cropped_image = self.image.crop((0, 0, min_width, min_height))
        self.image = Image.new('RGB', (width, height), color=self.canvas_color)
        self.image.paste(cropped_image, (0, 0))
    
    def set_canvas_pixel_size(self, x, y):
        """
        Set the size of a pixel in the canvas
        """

        self.pixel_size_x = x
        self.pixel_size_y = y
        
    def set_canvas_pixel_size_x(self, x):
        """
        Set the x size of a pixel in the canvas
        """

        self.set_canvas_pixel_size(x, self.pixel_size_y)
    
    def set_canvas_pixel_size_y(self, y):
        """
        Set the y size of a pixel in the canvas
        """

        self.set_canvas_pixel_size(self.pixel_size_x, y)
        
    def resize_canvas(self):
        """
        Resize the canvas
        """

        self.image = self.image.resize(
            (
                self.image.width * int(self.pixel_size_x),
                self.image.height * int(self.pixel_size_y)
            ), resample=Image.NEAREST)
        
    def canvas_crop(self):
        """
        Crop the canvas
        """

        # ! send a tuple
        self.image = self.image.crop((
            self.start_pixel_x,
            self.start_pixel_y,
            self.end_pixel_x,
            self.end_pixel_y
            ))
        
        
    def set_canvas_start_pixel(self, x, y):
        """
        Set the start pixel of the canvas on x and y axis
        """

        self.start_pixel_x = x
        self.start_pixel_y = y
        
    def set_canvas_start_pixel_x(self, x):
        """
        Set the start pixel of the canvas on x axis
        """

        self.set_canvas_start_pixel(x, self.start_pixel_y)
        
    def set_canvas_start_pixel_y(self, y):
        """
        Set the start pixel of the canvas on y axis
        """

        self.set_canvas_start_pixel(self.start_pixel_x, y)
    
    def set_canvas_end_pixel(self, x, y):
        """
        Set the end pixel of the canvas on x and y axis
        """

        self.end_pixel_x = x
        self.end_pixel_y = y
        
    def set_canvas_end_pixel_x(self, x):
        """
        Set the end pixel of the canvas on x axis
        """

        self.set_canvas_end_pixel(x, self.end_pixel_y)
        
    def set_canvas_end_pixel_y(self, y):
        """
        Set the end pixel of the canvas on y axis
        """

        self.set_canvas_end_pixel(self.end_pixel_x, y)
        
    def fill_canvas(self, color):
        """
        Fill the canvas with the given color
        """
        self.set_canvas_color(color)
        self.image.paste(self.canvas_color, (0, 0, self.image.width, self.image.height))
    
    def draw_line(self, size):
        """
        Draw a line of the given size, using the current color, starting from the current cursor position, wrapping around if necessary
        """

        for i in range(int(size)):
            self.check_cursor_out_of_image()
            self.image.putpixel((self.cursor_x, self.cursor_y), self.color)
            self.move_cursor_forward()

def fetch_string(bytecode, addr):
    """
    Fetch a null-terminated string from the bytecode
    Returns the string and the address past the string
    """
    string = ''
    while True:
        char = bytecode[addr]
        addr += 1
        if char == 0:
            break
        string += chr(char)
    return string, addr


# Dictionary of functions that handle each opcode
# The functions take the VM as a parameter
handlers = {}

def run(bytecode):
    """
    Run the given bytecode
    Input: bytecode
    Output: PIL image
    """
    
    vm = VM(bytecode)

    while True:
        opcode_byte = bytecode[vm.addr]
        opcode = Opcode(opcode_byte)
        vm.addr += 1

        if opcode not in handlers:
            raise LangRuntimeError('Unknown opcode: %s' % opcode)
        
        status = handlers[opcode](vm)
        if status == 'end':
            break
    
    # Program has ended without errors
    vm.canvas_crop()
    vm.resize_canvas()
    return vm.image

# Past this point are the functions that handle each opcode

@add_to_dict(handlers, Opcode.END)
def handle(vm):
    """
    Handle the END opcode (end of program)
    """
    return 'end'

@add_to_dict(handlers, Opcode.ACC_PUSH)
def handle(vm):
    """
    Handle the ACC_PUSH opcode (push a value to the accumulator)
    """

    value = struct.unpack('f', vm.bytecode[vm.addr:vm.addr+4])[0]
    vm.addr += 4
    vm.acc.append(value)

@add_to_dict(handlers, Opcode.ACC_POP)
def handle(vm):
    """
    Handle the ACC_POP opcode (pop a value from the accumulator)
    """

    vm.acc.pop()

@add_to_dict(handlers, Opcode.GVAR_DECL)
def handle(vm):
    """
    Handle the GVAR_DECL opcode (declare a global variable)
    """

    var_name, vm.addr = fetch_string(vm.bytecode, vm.addr)
    vm.global_vars[var_name] = vm.acc.pop()

@add_to_dict(handlers, Opcode.GVAR_SET)
def handle(vm):
    """
    Handle the GVAR_SET opcode (set a global variable)
    """

    var_name, vm.addr = fetch_string(vm.bytecode, vm.addr)
    if var_name not in vm.global_vars:
        raise LangRuntimeError('Cannot assign to undeclared global variable %s' % var_name)
    vm.global_vars[var_name] = vm.acc[-1]

@add_to_dict(handlers, Opcode.GVAR_GET)
def handle(vm):
    """
    Handle the GVAR_GET opcode (get a global variable)
    """

    var_name, vm.addr = fetch_string(vm.bytecode, vm.addr)
    if var_name not in vm.global_vars:
        raise LangRuntimeError('Cannot read undeclared global variable %s' % var_name)
    vm.acc.append(vm.global_vars[var_name])

@add_to_dict(handlers, Opcode.LVAR_DECL)
def handle(vm):
    """
    Handle the LVAR_DECL opcode (declare a local variable)
    """

    var_name, vm.addr = fetch_string(vm.bytecode, vm.addr)
    vm.local_vars_stack[-1][var_name] = vm.acc.pop()

@add_to_dict(handlers, Opcode.LVAR_SET)
def handle(vm):
    """
    Handle the LVAR_SET opcode (set a local variable)
    """

    var_name, vm.addr = fetch_string(vm.bytecode, vm.addr)
    if var_name not in vm.local_vars_stack[-1]:
        raise LangRuntimeError('Cannot assign to undeclared local variable %s' % var_name)
    vm.local_vars_stack[-1][var_name] = vm.acc[-1]

@add_to_dict(handlers, Opcode.LVAR_GET)
def handle(vm):
    """
    Handle the LVAR_GET opcode (get a local variable)
    """

    var_name, vm.addr = fetch_string(vm.bytecode, vm.addr)
    if var_name not in vm.local_vars_stack[-1]:
        raise LangRuntimeError('Cannot read undeclared local variable %s' % var_name)
    vm.acc.append(vm.local_vars_stack[-1][var_name])

@add_to_dict(handlers, Opcode.CALL)
def handle(vm):
    """
    Handle the CALL opcode (call a function)
    """

    vm.local_vars_stack.append({})
    goto_addr = int.from_bytes(vm.bytecode[vm.addr:vm.addr+4], 'little')
    vm.addr += 4
    nb_params = int.from_bytes(vm.bytecode[vm.addr:vm.addr+4], 'little')
    vm.addr += 4
    params = []
    for i in range(nb_params):
        params.append(vm.acc.pop())
    # Add the return vm.address to the accumulator first
    vm.acc.append(vm.addr)
    # Then push the parameters
    for param in params:
        vm.acc.append(param)
    # Then add the number of parameters to the accumulator
    vm.acc.append(float(nb_params))
    # Finally, jump to the function
    vm.addr = goto_addr

@add_to_dict(handlers, Opcode.NAT_CALL)
def handle(vm):
    """
    Handle the NAT_CALL opcode (call a native function)
    """

    func_name, vm.addr = fetch_string(vm.bytecode, vm.addr)
    nb_params = int.from_bytes(vm.bytecode[vm.addr:vm.addr+4], 'little')
    vm.addr += 4
    params = []
    for i in range(nb_params):
        params.append(vm.acc.pop())
    func_elt = NATIVE_FUNCTIONS[func_name]
    func = func_elt[0]
    nb_args = func_elt[1]
    if nb_args != nb_params:
        raise LangRuntimeError('Native function called with %d arguments, expected %d' % (nb_params, nb_args))
    # Call the native function
    params.reverse()
    result = func(vm, *params)
    # Push the result onto the accumulator
    vm.acc.append(result)

@add_to_dict(handlers, Opcode.RETURN)
def handle(vm):
    """
    Handle the RETURN opcode (return from a function)
    """

    vm.local_vars_stack.pop()
    # Keep the return value
    return_value = vm.acc.pop()
    # Go back to the caller
    vm.addr = vm.acc.pop()
    # Put back the return value
    vm.acc.append(return_value)

@add_to_dict(handlers, Opcode.GOTO)
def handle(vm):
    """
    Handle the GOTO opcode (jump to a vm.address)
    """

    vm.addr = int.from_bytes(vm.bytecode[vm.addr:vm.addr+4], 'little')

@add_to_dict(handlers, Opcode.ELSE_GO)
def handle(vm):
    """
    Handle the ELSE_GO opcode (jump to a vm.address if the accumulator is empty)
    """

    if not vm.acc.pop():
        vm.addr = int.from_bytes(vm.bytecode[vm.addr:vm.addr+4], 'little')
    else:
        vm.addr += 4

@add_to_dict(handlers, Opcode.ADD)
def handle(vm):
    """
    Handle the ADD opcode (addition)
    """

    y = vm.acc.pop()
    x = vm.acc.pop()
    vm.acc.append(x + y)

@add_to_dict(handlers, Opcode.SUB)
def handle(vm):
    """
    Handle the SUB opcode (subtraction)
    """

    y = vm.acc.pop()
    x = vm.acc.pop()
    vm.acc.append(x - y)

@add_to_dict(handlers, Opcode.MUL)
def handle(vm):
    """
    Handle the MUL opcode (multiplication)
    """

    y = vm.acc.pop()
    x = vm.acc.pop()
    vm.acc.append(x * y)

@add_to_dict(handlers, Opcode.DIV)
def handle(vm):
    """
    Handle the DIV opcode (division)
    """

    y = vm.acc.pop()
    x = vm.acc.pop()

    if y == 0:
        raise LangRuntimeError('Division by zero')

    vm.acc.append(x / y)

@add_to_dict(handlers, Opcode.MOD)
def handle(vm):
    """
    Handle the MOD opcode (modulo)
    """

    y = vm.acc.pop()
    x = vm.acc.pop()
    
    if y == 0:
        raise LangRuntimeError('Division by zero')

    vm.acc.append(x % y)

@add_to_dict(handlers, Opcode.EQ)
def handle(vm):
    """
    Handle the EQ opcode (equality)
    """

    y = vm.acc.pop()
    x = vm.acc.pop()
    vm.acc.append(float(x == y))

@add_to_dict(handlers, Opcode.NEQ)
def handle(vm):
    """
    Handle the NEQ opcode (inequality)
    """

    y = vm.acc.pop()
    x = vm.acc.pop()
    vm.acc.append(float(x != y))

@add_to_dict(handlers, Opcode.GT)
def handle(vm):
    """
    Handle the GT opcode (greater than)
    """

    y = vm.acc.pop()
    x = vm.acc.pop()
    vm.acc.append(float(x > y))

@add_to_dict(handlers, Opcode.GTE)
def handle(vm):
    """
    Handle the GTE opcode (greater than or equal to)
    """

    y = vm.acc.pop()
    x = vm.acc.pop()
    vm.acc.append(float(x >= y))

@add_to_dict(handlers, Opcode.LT)
def handle(vm):
    """
    Handle the LT opcode (less than)
    """

    y = vm.acc.pop()
    x = vm.acc.pop()
    vm.acc.append(float(x < y))

@add_to_dict(handlers, Opcode.LTE)
def handle(vm):
    """
    Handle the LTE opcode (less than or equal to)
    """

    y = vm.acc.pop()
    x = vm.acc.pop()
    vm.acc.append(float(x <= y))

@add_to_dict(handlers, Opcode.NOT)
def handle(vm):
    """
    Handle the NOT opcode (logical not)
    """

    vm.acc.append(float(not vm.acc.pop()))

@add_to_dict(handlers, Opcode.NEG)
def handle(vm):
    """
    Handle the NEG opcode (negation)
    """

    vm.acc.append(-vm.acc.pop())

@add_to_dict(handlers, Opcode.AND)
def handle(vm):
    """
    Handle the AND opcode (logical and)
    """

    y = vm.acc.pop()
    x = vm.acc.pop()
    vm.acc.append(float(x and y))

@add_to_dict(handlers, Opcode.OR)
def handle(vm):
    """
    Handle the OR opcode (logical or)
    """

    y = vm.acc.pop()
    x = vm.acc.pop()
    vm.acc.append(float(x or y))

@add_to_dict(handlers, Opcode.FN_CHECK)
def handle(vm):
    """
    Handle the FN_CHECK opcode (check the number of arguments passed to a function)
    """

    nb_args = vm.acc.pop()
    nb_params = vm.acc.pop()
    if nb_args != nb_params:
        raise LangRuntimeError('Function called with %d arguments, expected %d' % (nb_params, nb_args))
