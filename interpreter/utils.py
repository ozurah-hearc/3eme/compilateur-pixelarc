"""
This module contains some utility functions and classes.
"""

class LangRuntimeError(Exception):
    """
    This class is used to raise errors in the interpreter.
    """
    
    pass
